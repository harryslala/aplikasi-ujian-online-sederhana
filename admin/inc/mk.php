<?php
include "../koneksi/konek.php";
session_start();
 if (empty($_SESSION['username']) AND empty($_SESSION['password'])){
 echo '
	<html>
		<head>
		<title>Forbidden Access</title>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<meta HTTP-EQUIV="REFRESH" content="3; url=../">
		<link href="style.css" rel="stylesheet" type="text/css" />
		</head>
		<body>
		<div id="all">
		<div id="main">
		<div class="centerblock">
		<div class="register stepbystep container content-block">
		<div class="body">
		<h2>Akses Di Tolak</h2>
		<p>Anda Tidak Berhak Mengakses Halaman Ini. Halaman Akan Redirect Dalam 3 Detik</p>
		</div>
		</div>
		</div>
		</div>
		</div>
		</body>
		</html>
  ';
}
else{
switch($_GET['act']){
	default:
	?>
	<div id="space" style="padding-top: 180px;"></div>
	<div class="tabelis" style="width:860px; margin: 0 auto;">
	<a href="add-mk" class="btn">Tambah Mata Kuliah</a>
		  <br />
		  <br />
		  <br />
		<table class="table table-striped table-condensed">
		<thead>
		<tr>
			<th>No.</th>
			<th>ID MK</th>
			<th>Kode MK</th>
			<th>Nama MK</th>
			<th>Aksi</th>
		</tr>
		</thead>
		<tbody>
		<?php 
			$tampil	=	mysqli_query($con, "SELECT * FROM tbl_mk ORDER BY id_mk DESC");
			$no=1;
		while ($r=mysqli_fetch_array($tampil)){
			?>
		<tr>
			<td><?= $no; ?></td>
			<td><?= $r['id_mk'] ?></td>
			<td><?= $r['kode_mk'] ?></td>
			<td><?= $r['nama_mk'] ?></td>
			<td><a href="edit-mk-<?=$r['id_mk'] ?>">Edit</a> | <a href="inc/del-mk-<?=$r['id_mk'] ?>">Hapus</a></td>
		</tr>
			<?php
			$no++;
		}
		?>
		</tbody>
		</table>
	</div>
	<?php
	break;
	case "add-mk":
	?>
	<div id="space" style="padding-top: 180px;"></div>
	<div class="tabelis" style="width:860px; margin: 0 auto;">
	<form class="form-horizontal" action="inc/aksimk.php?detail=add-mk&act=input" method="post">
		<fieldset>
		<legend>Tambah Mata Kuliah</legend>
		<!-- Text input-->
		<div class="control-group">
		  <div class="controls">
			<input id="kode_mk" name="kode_mk" placeholder="Kode Mata Kuliah" class="input-xlarge" required="" type="text">
			<input id="nama_mk" name="nama_mk" placeholder="Nama Mata Kuliah" class="input-xlarge" required="" type="text">
		  </div>
		</div>

		<!-- Button -->
		<div class="control-group">
		  <label class="control-label" for=""></label>
		  <div class="controls">
			<button id="" name="" class="btn btn-success">Simpan</button>
		  </div>
		</div>
		</fieldset>
		</form>
	</div>
	<?php
	break;
	case "edit-mk":
	?>	
	<?php 
	$edit	=	mysqli_query($con, "SELECT * FROM tbl_mk WHERE id_mk='$_GET[id]'");
    $r		=	mysqli_fetch_array($edit);
	?>
		<div id="space" style="padding-top: 180px;"></div>
	<div class="tabelis" style="width:860px; margin: 0 auto;">
	<form class="form-horizontal" action="inc/aksimk.php?detail=edit-mk&act=edit" method="post">
		<fieldset>
		<legend>Tambah Mata Kuliah</legend>
		<!-- Text input-->
		<div class="control-group">
		  <label class="control-label" for="mk">Mata Kuliah</label>
		  <div class="controls">
			<input id="id_mk" name="id_mk" value="<?=$r['id_mk'] ?>" type="hidden">
			<input id="nama_mk" name="nama_mk" value="<?=$r['nama_mk'] ?>" class="input-xlarge"  type="text">
			<p class="help-block">Update Mata Kuliah</p>
		  </div>
		</div>

		<!-- Button -->
		<div class="control-group">
		  <label class="control-label" for=""></label>
		  <div class="controls">
			<button id="" name="" class="btn btn-success">Update</button>
		  </div>
		</div>
		</fieldset>
		</form>
	</div>
	<?php
	break;
}
}
?>