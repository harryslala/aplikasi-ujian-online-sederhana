<?php
include "../koneksi/konek.php";
session_start();
 if (empty($_SESSION['username']) AND empty($_SESSION['password'])){
 echo '
	<html>
		<head>
		<title>Forbidden Access</title>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<meta HTTP-EQUIV="REFRESH" content="3; url=../">
		<link href="style.css" rel="stylesheet" type="text/css" />
		</head>
		<body>
		<div id="all">
		<div id="main">
		<div class="centerblock">
		<div class="register stepbystep container content-block">
		<div class="body">
		<h2>Akses Di Tolak</h2>
		<p>Anda Tidak Berhak Mengakses Halaman Ini. Halaman Akan Redirect Dalam 3 Detik</p>
		</div>
		</div>
		</div>
		</div>
		</div>
		</body>
		</html>
  ';
}
else{

switch($_GET['act']){
	default:
	?>
	<div id="space" style="padding-top: 180px;"></div>
	<div class="tabelis" style="width:860px; margin: 0 auto;">
	<a href="add-materi" class="btn">Tambah Materi Belajar</a>
		  <br />
		  <br />
		  <br />
		<table class="table table-striped table-condensed">
		<thead>
		<tr>
			<th>No.</th>
			<th>Nama Materi</th>
			<th>File</th>
			<th>Keterangan</th>
			<th>Aksi</th>
		</tr>
		</thead>
		<tbody>
		<?php 
			$tampil	=	mysqli_query($con, "SELECT * FROM tbl_materi ORDER BY id_materi DESC");
			$no=1;
		while ($r=mysqli_fetch_array($tampil)){
			?>
		<tr>
			<td><?= $no; ?></td>
			<td><?= $r['nama_materi'] ?></td>
			<td><a href="../file/<?= $r['file'] ?>"><?= $r['file'] ?></a></td>
			<td><?= $r['keterangan'] ?></td>
			<td><a href="inc/del-materi-<?=$r['id_materi'] ?>">Hapus</a></td>
		</tr>
			<?php
			$no++;
		}
		?>
		</tbody>
		</table>
	</div>
	<div id="space" style="padding-top: 100px;"></div>
	<?php
	break;
	case "add-materi":
	?>
	<div id="space" style="padding-top: 180px;"></div>
	<div class="tabelis" style="width:860px; margin: 0 auto;">
		<form class="form-horizontal" action="inc/add-materi" method="post" enctype='multipart/form-data'>
		<fieldset>
		<legend>Tambah Materi Pembelajaran</legend>
		<div class="control-group">
		  <label class="control-label" for="materi">Masukan Materi</label>
		  <div class="controls">
			<input id="materi" name="materi" required="required" placeholder="Nama Materi" class="input-xlarge" type="text">
			
		  </div>
		</div>

		<!-- File Button --> 
		<div class="control-group">
		  <label class="control-label" for="file">File Materi</label>
		  <div class="controls">
			<input id="file" name="file" required="required" class="input-file" type="file">
		  </div>
		</div>

		<!-- Textarea -->
		<div class="control-group">
		  <label class="control-label" for="keterangan">Keterangan Materi</label>
		  <div class="controls">                     
			<textarea id="keterangan" name="keterangan" required="required"></textarea>
		  </div>
		</div>

		<!-- Button -->
		<div class="control-group">
		  <label class="control-label" for="simpan"></label>
		  <div class="controls">
			<button id="simpan" name="simpan" class="btn btn-success">Simpan</button>
		  </div>
		</div>
		
		</fieldset>
		</form>
	</div>
	<div id="space" style="padding-top: 100px;"></div>
	<?php
	break;
}
}
?>