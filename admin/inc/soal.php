<?php
session_start();
 if (empty($_SESSION['username']) AND empty($_SESSION['password'])){
 echo '
	<html>
		<head>
		<title>Forbidden Access</title>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<meta HTTP-EQUIV="REFRESH" content="3; url=../">
		<link href="style.css" rel="stylesheet" type="text/css" />
		</head>
		<body>
		<div id="all">
		<div id="main">
		<div class="centerblock">
		<div class="register stepbystep container content-block">
		<div class="body">
		<h2>Akses Di Tolak</h2>
		<p>Anda Tidak Berhak Mengakses Halaman Ini. Halaman Akan Redirect Dalam 3 Detik</p>
		</div>
		</div>
		</div>
		</div>
		</div>
		</body>
		</html>
  ';
}
else{
include "../koneksi/konek.php";
switch($_GET['act']){
	default:
	?>
	<div id="space" style="padding-top: 180px;"></div>
	<div class="tabelis" style="width:860px; margin: 0 auto;">
	<a href="add-soal" class="btn">Tambah Soal Kuis</a>
		  <br />
		  <br />
		  <br />
		<table class="table table-striped table-condensed">
		<thead>
		<tr>
			<th>No.</th>
			<th>Mata Kuliah</th>
			<th>Aksi</th>
		</tr>
		</thead>
		<tbody>
		<?php 
			$tampil	=	mysqli_query($con, "SELECT tbl_mk.id_mk,tbl_mk.nama_mk FROM tbl_soal,tbl_mk WHERE tbl_mk.id_mk = tbl_soal.id_mk GROUP BY tbl_mk.nama_mk");
			$no=1;
		while ($r=mysqli_fetch_array($tampil)){
			?>
		<tr>
			<td><?= $no; ?></td>
			<td><?= $r['nama_mk'] ?></td>
			<td><a href="view-soal-<?=$r['id_mk'] ?>">Lihat Soal</a></td>
		</tr>
			<?php
			$no++;
		}
		?>
		</tbody>
		</table>
	</div>
	<?php
	break;
	case "add-soal":
	?>
	<div id="space" style="padding-top: 180px;"></div>
	<div class="tabelis" style="width:860px; margin: 0 auto;">
		<form class="form-horizontal" action="inc/aksisoal.php?detail=add-soal&act=input" method="post">
			<fieldset>
			<legend>Tambah Soal Kuis</legend>
			<div class="control-group">
			  <label class="control-label" for="mapel">Pilih Mata Kuliah</label>
			  <div class="controls">
			  <select id="idmk" name="idmk" class="input-xlarge">
				<?php
					$tampil	= mysqli_query($con, "SELECT * FROM tbl_mk");
					while ($r = mysqli_fetch_array($tampil)){
						?>
						<option value="<?=$r['id_mk']?>"><?=$r['nama_mk'] ?></option>
						<?php
					}
				?>
				</select>
			  </div>
			</div>
			
			<div class="control-group">
			  <label class="control-label" for="tanya">Soal Pertanyaan</label>
			  <div class="controls">                     
				<textarea id="tanya" name="tanya"></textarea>
			  </div>
			</div>

			<div class="control-group">
			  <label class="control-label" for="piliha">Pilihan A</label>
			  <div class="controls">
				<input id="piliha" name="piliha" placeholder="" class="input-xlarge" required="" type="text">
				
			  </div>
			</div>

			<div class="control-group">
			  <label class="control-label" for="pilihb">Pilihan B</label>
			  <div class="controls">
				<input id="pilihb" name="pilihb" placeholder="" class="input-xlarge" required="" type="text">
				
			  </div>
			</div>

			<div class="control-group">
			  <label class="control-label" for="pilihc">Pilihan C</label>
			  <div class="controls">
				<input id="pilihc" name="pilihc" placeholder="" class="input-xlarge" required="" type="text">
				
			  </div>
			</div>

			<div class="control-group">
			  <label class="control-label" for="pilihd">Pilihan D</label>
			  <div class="controls">
				<input id="pilihd" name="pilihd" placeholder="" class="input-xlarge" required="" type="text">
				
			  </div>
			</div>

			<div class="control-group">
			  <label class="control-label" for="kunci">Kunci Jawaban</label>
			  <div class="controls">
				<input id="kunci" name="kunci" placeholder="" class="input-xlarge" required="" type="text">
				
			  </div>
			</div>

			<div class="control-group">
			  <label class="control-label" for="simpan"></label>
			  <div class="controls">
				<button type="submit" id="simpan" name="simpan" class="btn btn-success">Simpan</button>
				<button type="reset" id="batal" name="batal" class="btn btn-danger">Batal</button>
			  </div>
			</div>
		</fieldset>
		</form>
	</div>
	</div>
	<?php
	break;
	case "view-soal":
	?>
	<div id="space" style="padding-top: 180px;"></div>
	<div class="tabelis" style="width:860px; margin: 0 auto;">
	<a href="add-soal" class="btn">Tambah Soal Kuis</a>
		  <br />
		  <br />
		  <br />
		<table class="table table-striped table-condensed">
		<thead>
		<tr>
			<th>No.</th>
			<th>Mata Kuliah</th>
			<th>Pertanyaan</th>
			<th>Pilihan A</th>
			<th>Pilihan B</th>
			<th>Pilihan C</th>
			<th>Pilihan D</th>
			<th>Kunci Jawaban</th>
			<th>Aksi</th>
		</tr>
		</thead>
		<tbody>
		<?php 
			$tampil	=	mysqli_query($con, "SELECT tbl_mk.nama_mk, tbl_soal.id_soal, tbl_soal.pertanyaan, tbl_soal.pilihan_a, tbl_soal.pilihan_b,
			tbl_soal.pilihan_c, tbl_soal.pilihan_d, tbl_soal.kunci FROM tbl_soal, tbl_mk WHERE tbl_soal.id_mk = 
			tbl_mk.id_mk AND tbl_soal.id_mk = $_GET[id]");
			$no=1;
		while ($r=mysqli_fetch_array($tampil)){
			?>
		<tr>
			<td><?= $no; ?></td>
			<td><?= $r['nama_mk'] ?></td>
			<td><?= $r['pertanyaan'] ?></td>
			<td><?= $r['pilihan_a'] ?></td>
			<td><?= $r['pilihan_b'] ?></td>
			<td><?= $r['pilihan_c'] ?></td>
			<td><?= $r['pilihan_d'] ?></td>
			<td><?= $r['kunci'] ?></td>
			<td><a href="inc/aksisoal.php?detail=del-soal&act=hapus&id=<?=$r['id_soal'] ?>">Hapus</a> &nbsp;<a href="home.php?detail=soal&act=edit-soal&id=<?=$r['id_soal'] ?>">Edit</a> </td>
		</tr>
			<?php
			$no++;
		}
		?>
		</tbody>
		</table>
	</div>
	<div id="space" style="padding-top: 80px;"></div>
	<?php
	break;
	case "edit-soal":
	?>
	<?php
	$edit	=	mysqli_query($con, "SELECT * FROM tbl_soal WHERE id_soal='$_GET[id]'");
    $r		=	mysqli_fetch_array($edit);
	?>
	<div id="space" style="padding-top: 180px;"></div>
	<div class="tabelis" style="width:860px; margin: 0 auto;">
		<form class="form-horizontal" action="inc/aksisoal.php?detail=edit-soal&act=edit" method="post">
			<fieldset>
			<legend>Tambah Soal Kuis</legend>
			<div class="control-group">
			  <label class="control-label" for="mk">Pilih Mata Kuliah</label>
			  <div class="controls">
			  <input type="hidden" name="id_soal" id="id_soal" value="<?=$r['id_soal'] ?>" />
			  <input id="id_mk" name="id_mk" value="<?=$r['id_mk'] ?>" type="hidden">
			  <?php
				$mak = mysqli_query($con, "SELECT tbl_mk.nama_mk FROM tbl_mk, 
				tbl_soal WHERE tbl_soal.id_mk = tbl_mk.id_mk AND tbl_soal.id_mk = '$r[id_mk]' GROUP BY tbl_mk.nama_mk");
				$t		=	mysqli_fetch_array($mk);
			  ?>
			  <input id="mk" name="mk" value="<?=$t['nama_mk'] ?>" disabled="disabled" class="input-xlarge" required="" type="text">
			  </div>
			</div>
			
			<div class="control-group">
			  <label class="control-label" for="tanya">Soal Pertanyaan</label>
			  <div class="controls">                     
				<textarea id="tanya" name="tanya"><?=$r['pertanyaan'] ?></textarea>
			  </div>
			</div>

			<div class="control-group">
			  <label class="control-label" for="piliha">Pilihan A</label>
			  <div class="controls">
				<input id="piliha" name="piliha" value="<?=$r['pilihan_a'] ?>" class="input-xlarge" required="" type="text">
			  </div>
			</div>

			<div class="control-group">
			  <label class="control-label" for="pilihb">Pilihan B</label>
			  <div class="controls">
				<input id="pilihb" name="pilihb" value="<?=$r['pilihan_b'] ?>" class="input-xlarge" required="" type="text">
				
			  </div>
			</div>

			<div class="control-group">
			  <label class="control-label" for="pilihc">Pilihan C</label>
			  <div class="controls">
				<input id="pilihc" name="pilihc" value="<?=$r['pilihan_c'] ?>" class="input-xlarge" required="" type="text">
				
			  </div>
			</div>

			<div class="control-group">
			  <label class="control-label" for="pilihd">Pilihan D</label>
			  <div class="controls">
				<input id="pilihd" name="pilihd" value="<?=$r['pilihan_d'] ?>" class="input-xlarge" required="" type="text">
				
			  </div>
			</div>

			<div class="control-group">
			  <label class="control-label" for="kunci">Kunci Jawaban</label>
			  <div class="controls">
				<input id="kunci" name="kunci" value="<?=$r['kunci'] ?>" class="input-xlarge" required="" type="text">
				
			  </div>
			</div>

			<div class="control-group">
			  <label class="control-label" for="simpan"></label>
			  <div class="controls">
				<button type="submit" id="simpan" name="simpan" class="btn btn-success">Simpan</button>
				<button type="reset" id="batal" name="batal" class="btn btn-danger">Batal</button>
			  </div>
			</div>
		</fieldset>
		</form>
	</div>
	</div>
	<?php
	break;
}
}
?>