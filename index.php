<!DOCTYPE html>
<head>
	<title>Go Practice!</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
    
    <!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
    <!-- Stylesheets -->
    <link rel="stylesheet" href="css/basdat.css">
    <link href='http://fonts.googleapis.com/css?family=Ruda' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
    
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
    
    <noscript><link rel="stylesheet" href="css/no-js.css"></noscript>
    
    <!-- Favicons -->
	<link rel="shortcut icon" href="images/favicon2.png">
    
    <!-- JavaScript -->
    <script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
    <script type='text/javascript' src='js/bootstrap.min.js'></script>
    <script type="text/javascript" src="js/jquery-easing.js"></script>
    <script type='text/javascript' src='js/jquery.placeholder.min.js'></script>
	<script type='text/javascript' src='js/jquery.flexslider-min.js'></script>
    <script type="text/javascript" src="js/main.js"></script>
    
</head>

<body>

 <div id="header">
  <div class="container">
   <div class="row-fluid">
    
    <div class="span1"></div>
    <nav class="nav-menu span10">
     <ul>
      <li style="float: left;"><a href="#">Home</a></li>
      <li><a href="#">Tentang</a></li>
      <li><a href="#contact">Contact</a></li>
     </ul>
    </nav>
    <div class="span1"></div>
    
   </div> <!-- End Row Fluid -->
  </div> <!-- End Container -->
 </div> <!-- End Header -->
 
 <div id="slider">
    <ul class="slides">
     <li>
      <p>
       <a href="register" class="btn-link btn-link-half">Daftar</a>
       <a href="login" class="btn-link btn-link-half">Login</a>
      </p>
     </li>
    </ul>
 </div> <!-- End Slider -->
</body>
</html>