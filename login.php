<!DOCTYPE html>
<html>
	<head>
	<title>Login - GO Practice!</title>

	<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="all">
	<div id="main">
	<div class="centerblock">
	<div class="register stepbystep container content-block">
	<div class="body">
		<form action="user/auth" method="post" name="reg_form" id="reg_form" class="forms">
			<div class="section fullwidth">
				<div>
					<label for="username">USERNAME*</label>
					<input name="username" type="text" value="" id="username" placeholder="Type Username" required>
				</div>
				<div>
					<label for="password">PASSWORD*</label>
					<input name="password" type="password" value="" id="password" placeholder="Type Password" required>
					<input type="hidden" name="idUser" type="text" value="" id="">				
				</div>
			</div>
			<div class="actions">
				<button name="next" class="bbutton">LOGIN</button>
			</div>
			<div class="actions">
				<center><a href="../basdat/">Home</a></center>
			</div>
		</form>
	</div>
	</div>
	</div>
	</div>
</div>
</body>
</html>