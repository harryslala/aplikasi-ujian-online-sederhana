<?php
session_start();
error_reporting(0);


if($_SESSION['login']==0){
  echo '
		<html>
		<head>
		<title>Login Berhasil</title>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<meta HTTP-EQUIV="REFRESH" content="3; url=../">
		<link href="style.css" rel="stylesheet" type="text/css" />
		</head>
		<body>
		<div id="all">
		<div id="main">
		<div class="centerblock">
		<div class="register stepbystep container content-block">
		<div class="body">
		<h2>Akses Di Tolak</h2>
		<p>Anda Berhasil Logout. Halaman Akan Redirect Dalam 3 Detik</p>
		</div>
		</div>
		</div>
		</div>
		</div>
		</body>
		</html>
  ';
}
else{
    if (empty($_SESSION['username']) AND empty($_SESSION['password']) AND $_SESSION['login']==0){
    echo '
            <html>
            <head>
            <title>Login Berhasil</title>
            <meta http-equiv="content-type" content="text/html;charset=utf-8" />
            <meta HTTP-EQUIV="REFRESH" content="3; url=../">
            <link href="style.css" rel="stylesheet" type="text/css" />
            </head>
            <body>
            <div id="all">
            <div id="main">
            <div class="centerblock">
            <div class="register stepbystep container content-block">
            <div class="body">
            <h2>Akses Di Tolak</h2>
            <p>Anda Berhasil Logout. Halaman Akan Redirect Dalam 3 Detik</p>
            </div>
            </div>
            </div>
            </div>
            </div>
            </body>
            </html>
    ';
    }
    else{
        include "../koneksi/konek.php";
        $query = mysqli_query($con, "SELECT * from tbl_user WHERE id_user=$_SESSION[idUser]");
        $r = mysqli_fetch_array($query);

        if ($r['status'] == "mahasiswa") {
            ?>
                <!DOCTYPE html>
                <head>
                <title>Rekap Nilai - GO Practice!</title>
                   
                <!-- Mobile Specific Meta -->
                <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
                 
                <!-- Stylesheets -->
                <link rel="stylesheet" href="css/basdat.css">
                <link href='http://fonts.googleapis.com/css?family=Ruda' rel='stylesheet' type='text/css'>
                <link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
                
                <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
                <![endif]-->
                  
                <noscript><link rel="stylesheet" href="css/no-js.css"></noscript>
                 
                <!-- Favicons -->
                <link rel="shortcut icon" href="images/favicon2.png">
                  
                <!-- JavaScript -->
                <script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
                <script type='text/javascript' src='js/bootstrap.min.js'></script>
                <script type="text/javascript" src="js/jquery-easing.js"></script>
                <script type='text/javascript' src='js/jquery.placeholder.min.js'></script>
                <script type='text/javascript' src='js/jquery.flexslider-min.js'></script>
                <script type="text/javascript" src="js/main.js"></script>
                  
                </head>

                <body>

                <div id="header">
                <?php
                    $query = mysqli_query($con, "SELECT * FROM tbl_profil WHERE id_user=$_SESSION[idUser]");
                    $r = mysqli_fetch_array($query);
                ?>
                <div class="container">
                <div class="row-fluid">
                    
                    <div class="span1"></div>
                    <nav class="nav-menu span10">
                    <ul>
                    <li style="float: left;"><a href="mhshome">Home</a></li>
                    <div class="dropdown">
                        <button class="dropbtn"><?php echo $r['nama_profil'] ?></button>
                            <div class="dropdown-content">
                                <ul>
                                <a href="profil">Profil</a>
                                <a href="logout">Logout</a>
                                </ul>
                            </div>
                    </div>
                    <li><a href="materi">Download Materi</a></li>
                    <li><a href="nilai">Rekap Nilai</a></li>
                    </ul>
                    </nav>
                    <div class="span1"></div>
                    
                </div> <!-- End Row Fluid -->
                </div> <!-- End Container -->
                </div> <!-- End Header -->
                <br />
                <br />
                <br />
                <div id="main">
                <div class="tabelis" style="width:860px; margin: 0 auto;margin-top: 40px;">
                <legend>Rekap Nilai Kuis</legend>
                <table class="table table-striped table-condensed">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama Kuis</th>
                        <th>Mata Kuliah</th>
                        <th>Jumlah Soal</th>
                        <th>Benar</th>
                        <th>Salah</th>
                        <th>Nilai</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php 
                        include "../../koneksi/konek.php";
                        $tampil	=	mysqli_query($con, "SELECT tn.id_user,tn.id_kuis,tn.benar,tn.salah,tn.nilai,tk.id_mk,tk.nama_kuis,tm.nama_mk,tk.jml_soal FROM tbl_nilai tn JOIN tbl_kuis tk ON tk.id_kuis=tn.id_kuis JOIN tbl_mk tm ON tm.id_mk=tk.id_mk WHERE tn.id_user='$_SESSION[idUser]'");
                        $no=1;
                    while ($r=mysqli_fetch_array($tampil)){
                        ?>
                    <tr>
                        <td><?= $no; ?></td>
                        <td><?= $r['nama_kuis'] ?></td>
                        <td><?= $r['nama_mk'] ?></a></td>
                        <td><?= $r['jml_soal'] ?></td>
                        <td style="color: red"><strong><?= $r['benar'] ?></strong></td>
                        <td style="color: red"><strong><?= $r['salah'] ?></strong></td>
                        <td style="color: red"><strong><?= $r['nilai'] ?></strong></td>
                    </tr>
                        <?php
                        $no++;
                    }
                    ?>
                    </tbody>
                    </table>
                <br />
                </div>
                </div> <!-- End Main -->
                <br />
                <br />
                <br />
                <div id="footer">
                <div class="container">
                
                <div class="copyright-text pull-left">Copyright &copy; 2013 by bye. All right!</div> <!-- Copyright Text -->
                
                <ul class="social pull-right">
                    <li><a href="#" title="Twitter"><img src="images/social/twitter.png" alt="Twitter" /></a></li>
                    <li><a href="#" title="Facebook"><img src="images/social/facebook500.png" alt="Facebook" /></a></li>
                    <li><a href="#" title="Google Plus"><img src="images/social/googleplus.png" alt="Google Plus" /></a></li>
                </ul> <!-- End Social Media -->
                
                </div> <!-- End Container -->
                </div> <!-- End Footer -->

                </body>
                </html>
            <?php
        }
        if ($r['status'] == "dosen") {
            ?>
                <!DOCTYPE html>
                <head>
                <title>Rekap Nilai Mahasiswa - GO Practice!</title>
                    
                <!-- Mobile Specific Meta -->
                <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
                    
                <!-- Stylesheets -->
                <link rel="stylesheet" href="css/basdat.css">
                <link href='http://fonts.googleapis.com/css?family=Ruda' rel='stylesheet' type='text/css'>
                <link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
                    
                <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
                <![endif]-->
                    
                <noscript><link rel="stylesheet" href="css/no-js.css"></noscript>
                    
                <!-- Favicons -->
                <link rel="shortcut icon" href="images/favicon2.png">
                    
                <!-- JavaScript -->
                <script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
                <script type='text/javascript' src='js/bootstrap.min.js'></script>
                <script type="text/javascript" src="js/jquery-easing.js"></script>
                <script type='text/javascript' src='js/jquery.placeholder.min.js'></script>
                <script type='text/javascript' src='js/jquery.flexslider-min.js'></script>
                <script type="text/javascript" src="js/main.js"></script>
                    
                </head>

                <body>

                <div id="header">
                <?php
                    $query = mysqli_query($con, "SELECT * FROM tbl_profil WHERE id_user=$_SESSION[idUser]");
                    $r = mysqli_fetch_array($query);
                ?>
                <div class="container">
                <div class="row-fluid">
                    
                    <div class="span1"></div>
                    <nav class="nav-menu span10">
                    <ul>
                    <li style="float:left;"><a href="doshome">Home</a></li>
                    <div class="dropdown">
                       <button class="dropbtn"><?php echo $r['nama_profil'] ?></button>
                           <div class="dropdown-content">
                               <ul>
                               <a href="profil">Profil</a>
                               <a href="logout">Logout</a>
                               </ul>
                           </div>
                    </div>
                    <li><a href="mkdos">Daftar Mata Kuliah</a></li>
                    <li><a href="soaldos">Daftar Kuis</a></li>
                    <li><a href="materidos">Daftar Materi</a></li>
                    <li><a href="nilai">Daftar Nilai</a></li> 
                    </ul>
                    </nav>
                    <div class="span1"></div>
                    
                </div> <!-- End Row Fluid -->
                </div> <!-- End Container -->
                </div> <!-- End Header -->
                <br />
                <br />
                <br />
                <div id="main">
                <div class="tabelis" style="width:860px; margin: 0 auto;margin-top: 40px;">
                <legend>Rekap Nilai Kuis Mahasiswa</legend>
                <table class="table table-striped table-condensed">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama</th>
                        <th>Nama Kuis</th>
                        <th>Mata Kuliah</th>
                        <th>Jumlah Soal</th>
                        <th>Benar</th>
                        <th>Salah</th>
                        <th>Nilai</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php 
                        include "../../koneksi/konek.php";
                        $tampil	=	mysqli_query($con, "SELECT tn.id_user,tn.id_kuis,tp.nama_profil,tn.benar,tn.salah,tn.nilai,tk.id_mk,tk.nama_kuis,tm.nama_mk,tk.jml_soal,tp.jurusan FROM tbl_nilai tn JOIN tbl_kuis tk ON tk.id_kuis=tn.id_kuis JOIN tbl_mk tm ON tm.id_mk=tk.id_mk JOIN tbl_profil tp ON tp.id_user=tn.id_user WHERE tp.jurusan='$_SESSION[jur]'");
                        $no=1;
                    while ($r=mysqli_fetch_array($tampil)){
                        ?>
                    <tr>
                        <td><?= $no; ?></td>
                        <td><?= $r['nama_profil'] ?></a></td>
                        <td><?= $r['nama_kuis'] ?></td>
                        <td><?= $r['nama_mk'] ?></td>
                        <td><?= $r['jml_soal'] ?></td>
                        <td style="color: red"><strong><?= $r['benar'] ?></strong></td>
                        <td style="color: red"><strong><?= $r['salah'] ?></strong></td>
                        <td style="color: red"><strong><?= $r['nilai'] ?></strong></td>
                    </tr>
                        <?php
                        $no++;
                    }
                    ?>
                    </tbody>
                    </table>
                <br />
                </div>
                </div> <!-- End Main -->
                <br />
                <br />
                <br />
                <div id="footer">
                <div class="container">
                
                <div class="copyright-text pull-left">Copyright &copy; 2013 by bye. All right!</div> <!-- Copyright Text -->
                
                <ul class="social pull-right">
                    <li><a href="#" title="Twitter"><img src="images/social/twitter.png" alt="Twitter" /></a></li>
                    <li><a href="#" title="Facebook"><img src="images/social/facebook500.png" alt="Facebook" /></a></li>
                    <li><a href="#" title="Google Plus"><img src="images/social/googleplus.png" alt="Google Plus" /></a></li>
                </ul> <!-- End Social Media -->
                
                </div> <!-- End Container -->
                </div> <!-- End Footer -->

                </body>
                </html>
                <?php
        }
    }
}
?>