<?php
	$url	=	$_GET['detail'];
	if($url == 'view-kuis'){
			?>
			<div id="space" style="padding-top: 80px;"></div>
			<div class="tabelis" style="width:860px; margin: 0 auto;">
				<legend>Kuis</legend>
				<table class="table table-striped table-condensed">
				<thead>
				<tr>
					<th>No.</th>
					<th>Nama Kuis</th>
					<th>Jumlah Soal</th>
					<th>Waktu Pengerjaan</th>
					<th>Tanggal</th>
					<th>Aksi</th>
				</tr>
				</thead>
				<tbody>
				<?php 		
				$tampil	= mysqli_query($con, "SELECT tbl_kuis.id_kuis,tbl_kuis.nama_kuis,tbl_kuis.jml_soal,tbl_kuis.nilai_soal,tbl_kuis.timer,tbl_kuis.tanggal,tbl_kuis.id_mk FROM tbl_kuis WHERE tbl_kuis.id_mk = $_POST[mk] AND tbl_kuis.id_user = $_POST[dosen]");
					$no=1;
				while ($r=mysqli_fetch_array($tampil)){
					?>
				<tr>
					<td><?= $no; ?></td>
					<td><?= $r['nama_kuis'] ?></td>
					<td><?= $r['jml_soal'] ?></td>
					<td><?= $r['timer'] ?> menit</td>
					<td><?= $r['tanggal'] ?></td>
					<?php
						$query = mysqli_query($con, "SELECT * FROM tbl_nilai WHERE id_user='$_SESSION[idUser]' AND id_kuis='$r[id_kuis]'");
						$find = mysqli_num_rows($query);

						if ($find > 0) {
							?>
							<td><a class="button" disabled>Mulai Kuiz</a></td>
							<?php
						} else {
							?>
							<td><a class="button" href="do-kuis-<?=$r['id_kuis'] ?>&no=1" onclick="return confirm('Are you sure want to start the quiz?')">Mulai Kuiz</a></td>
							<?php
						}
					?>
				</tr>
					<?php
					$no++;
				}
				?>
				</tbody>
				</table>
				<button id="" name="" class="btn btn-success" onclick="javascript:window.location='kuis';">Kembali</button>
			</div>
			<?php
	}
	else if($url	==	"do-kuis"){
		include "inc/test.php";
	}
	else if($url == "home") {
		?>
		<div id="space" style="padding-top: 80px;"></div>
		<div class="tabelis" style="width:560px; margin: 0 auto;">
		<form autocomplete="off" class="form-horizontal" action="view-kuis" method="post">
		<fieldset>
		<legend>Mulai Kuis</legend>
		<div class="control-group">
			<label class="control-label" for="mapel">Pilih Dosen</label>
			<div class="controls">
			<select id="dosen" name="dosen" class="input-xlarge" required>
					<option selected disabled hidden></option>
			<?php
				$tampil	= mysqli_query($con, "SELECT tbl_user.id_user,tbl_profil.nama_profil,tbl_profil.jurusan,tbl_user.status FROM tbl_profil, tbl_user WHERE tbl_profil.id_user=tbl_user.id_user AND tbl_profil.jurusan='$_SESSION[jur]' AND tbl_user.status = 'dosen' GROUP BY tbl_profil.nama_profil");
				while ($r = mysqli_fetch_array($tampil)){
					?>
					<option value="<?=$r['id_user']?>"><?=$r['nama_profil'] ?></option>
					<?php
				}
			?>
			</select>
			</div>
		</div>

		<div class="control-group">
			<label class="control-label" for="mapel">Pilih Mata Kuliah</label>
			<div class="controls">
			<select id="mk" name="mk" class="input-xlarge" required>
					<option selected disabled hidden></option>
			<?php
				$tampil	= mysqli_query($con, "SELECT tm.nama_mk, tm.id_mk, tm.id_user, tp.jurusan FROM tbl_mk tm JOIN tbl_profil tp ON tp.id_user=tm.id_user WHERE tp.jurusan='$_SESSION[jur]'");
				while ($r = mysqli_fetch_array($tampil)){
					?>
					<option value="<?=$r['id_mk']?>"><?=$r['nama_mk'] ?></option>
					<?php
				}
			?>
			</select>
			<input type="hidden" value="<?=$r['id_kuis']?>" name="idKuis" />
			</div>
		</div>
	
		<div class="control-group">
			<label class="control-label" for="simpan"></label>
			<div class="controls">
			<button type="submit" id="simpan" name="simpan" class="btn btn-success">Lihat Kuis</button>
			</div>
		</div>
	</fieldset>
	</form>
	</div>
		<?php
	}
?>
