<?php
session_start();
 if (empty($_SESSION['username']) AND empty($_SESSION['password'])){
 echo '
	<html>
		<head>
		<title>Forbidden Access</title>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<meta HTTP-EQUIV="REFRESH" content="3; url=../">
		<link href="style.css" rel="stylesheet" type="text/css" />
		</head>
		<body>
		<div id="all">
		<div id="main">
		<div class="centerblock">
		<div class="register stepbystep container content-block">
		<div class="body">
		<h2>Akses Di Tolak</h2>
		<p>Anda Tidak Berhak Mengakses Halaman Ini. Halaman Akan Redirect Dalam 3 Detik</p>
		</div>
		</div>
		</div>
		</div>
		</div>
		</body>
		</html>
  ';
}
else{

	switch($_GET['act']){
		default:
		?>
		<div id="space" style="padding-top: 80px;"></div>
	<div class="tabelis" style="width:640px; margin: 0 auto;">
	<form action="inc/cekuiz.php?detail=cek-soal&act=cek&no=<?= $_GET['no'] ?>" method="post">
		<table class="table table-striped table-condensed">
		<tbody>
		<?php 
			$tampil	= mysqli_query($con, "SELECT tbl_soal.id_soal, tbl_soal.no_soal, tbl_soal.pertanyaan, tbl_soal.pilihan_a, tbl_soal.pilihan_b,tbl_soal.pilihan_c, tbl_soal.pilihan_d, tbl_soal.kunci, tbl_kuis.id_kuis, tbl_kuis.jml_soal, tbl_kuis.nilai_soal FROM tbl_kuis JOIN tbl_soal ON tbl_soal.id_kuis=tbl_kuis.id_kuis WHERE tbl_soal.id_kuis = $_GET[id] AND tbl_soal.no_soal = $_GET[no]");
		while ($r=mysqli_fetch_array($tampil)){
			?>
		<thead>
		<tr>
			<input type="hidden" name="no_soal" value="<?= $r['no_soal'] ?>" />
			<input type="hidden" name="id_kuis" value="<?= $r['id_kuis'] ?>" />
			<input type="hidden" name="nilai_soal" value="<?= $r['nilai_soal'] ?>" />
			<input type="hidden" name="jml_soal" value="<?= $r['jml_soal'] ?>" />
    		<th>No. <?= $r['no_soal'] ?></th>
		  </tr>
		  </thead>
  		<tr>
    		<td colspan=3><?= $r['pertanyaan'] ?></td>
  		</tr>
  		<tr>
    		<td colspan=3><b>a.</b> <?= $r['pilihan_a'] ?></td>
  		</tr>
  		<tr>
   			<td colspan=3><b>b.</b> <?= $r['pilihan_b'] ?></td>
  		</tr>
  		<tr>
    		<td colspan=3><b>c.</b> <?= $r['pilihan_c'] ?></td>
  		</tr>
  		<tr>
    		<td colspan=3><b>d.</b> <?= $r['pilihan_d'] ?></td>
  		</tr>
  		<tr>
    		<td colspan=3>
			<input type="hidden" value="<?= $r['id_soal'] ?>" name="id_soal" />
			<select name="jawaban" required>
			<?php
				$tmp = mysqli_query($con, "SELECT pilihan_a,pilihan_b,pilihan_c,pilihan_d FROM tbl_soal WHERE id_soal = $r[id_soal]");
				while ($r = mysqli_fetch_array($tmp)){
				?>
				<option value="" selected disabled hidden>-- Pilih Jawaban --</option>	
				<option value="<?=$r['pilihan_a'] ?>"><?=$r['pilihan_a'] ?></option>	
				<option value="<?=$r['pilihan_b'] ?>"><?=$r['pilihan_b'] ?></option>	
				<option value="<?=$r['pilihan_c'] ?>"><?=$r['pilihan_c'] ?></option>	
				<option value="<?=$r['pilihan_d'] ?>"><?=$r['pilihan_d'] ?></option>	
				<?php
				}
			?>
			</select></td>
		  </tr>
		  <tr>
			  <td><div style="padding-top: 30px;"></div></td>
		  </tr>
		  <?php
			$no++;
		}
		?>
		</tbody>
		</table>	
		<input type="submit" name="submit" class="btn btn-success" value="Kirim" />
	</form>
	</div>
		<?php
		break;
	}

}
?>