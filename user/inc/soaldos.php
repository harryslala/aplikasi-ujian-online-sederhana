<?php
//include "../koneksi/konek.php";
session_start();
 if (empty($_SESSION['username']) AND empty($_SESSION['password'])){
 echo '
	<html>
		<head>
		<title>Forbidden Access</title>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<meta HTTP-EQUIV="REFRESH" content="3; url=../">
		<link href="style.css" rel="stylesheet" type="text/css" />
		</head>
		<body>
		<div id="all">
		<div id="main">
		<div class="centerblock">
		<div class="register stepbystep container content-block">
		<div class="body">
		<h2>Akses Di Tolak</h2>
		<p>Anda Tidak Berhak Mengakses Halaman Ini. Halaman Akan Redirect Dalam 3 Detik</p>
		</div>
		</div>
		</div>
		</div>
		</div>
		</body>
		</html>
  ';
}
else{
switch($_GET['act']){
	default:
	?>
	<div id="space" style="padding-top: 80px;"></div>
	<div class="tabelis" style="width:860px; margin: 0 auto;">
	<a href="buat-kuis" class="btn">Buat Kuis</a>
		  <br />
		  <br />
		  <br />
		<table class="table table-striped table-condensed">
		<thead>
		<tr>
			<th>No.</th>
			<th>Mata Kuliah</th>
			<th>Nama Kuis</th>
			<th>Jumlah Soal</th>
			<th>Waktu Pengerjaan</th>
			<th>Tanggal</th>
			<th>Aksi</th>
		</tr>
		</thead>
		<tbody>
		<?php 
			$tampil	= mysqli_query($con, "SELECT * FROM tbl_kuis tk, tbl_mk mk WHERE tk.id_mk=mk.id_mk AND tk.id_user=$_SESSION[idUser]");
			$no=1;
		while ($r=mysqli_fetch_array($tampil)){
			?>
		<tr>
			<td><?= $no; ?></td>
			<td><?= $r['nama_mk'] ?></td>
			<td><?= $r['nama_kuis'] ?></td>
			<td><?= $r['jml_soal'] ?></td>
			<td><?= $r['timer'] ?> menit</td>
			<td><?= $r['tanggal'] ?></td>
			<td><a class="button" href="view-soal-<?=$r['id_kuis'] ?>-<?=$r['jml_soal'] ?>?id=<?php echo $r['id_kuis'] ?>&j=<?php echo $r['jml_soal'] ?>">Lihat Soal</a></td>
		</tr>
			<?php
			$no++;
		}
		?>
		</tbody>
		</table>
	</div>
	<?php
	break;
	case "add-soal":
	$id_kuis=$_GET['id'];
	?>
	<div id="space" style="padding-top: 80px;"></div>
	<div autocomplete="off" class="tabelis" style="width:860px; margin: 0 auto;">
		<form class="form-horizontal" action="inc/aksisoaldos.php?detail=add-soal&act=input" method="post">
			<fieldset>
			<legend>Tambah Soal Kuis</legend>
			<?php
				$query = mysqli_query($con, "SELECT ts.*, tk.jml_soal FROM tbl_soal ts JOIN tbl_kuis tk ON tk.id_kuis=ts.id_kuis WHERE ts.id_kuis=$id_kuis");
				$no=mysqli_num_rows($query);
				$total = $no+1;
			?>
			<div class="control-group">
			  <label class="control-label" for="piliha">No</label>
			  <div class="controls">
				<input autocomplete="off" id="number" name="no_soal" placeholder="" class="input-xlarge" required="" type="number" value="<?= $total ?>">				
			  </div>
			</div>

			<div class="control-group">
			  <label class="control-label" for="tanya">Soal Pertanyaan</label>
			  <div class="controls">                     
				<textarea autocomplete="off" id="tanya" name="tanya" value=""></textarea>
			  </div>
			</div>

			<div class="control-group">
			  <label class="control-label" for="piliha">Pilihan A</label>
			  <div class="controls">
				<input autocomplete="off" id="piliha" name="piliha" placeholder="" class="input-xlarge" required="" type="text" value="">				
			  </div>
			</div>

			<div class="control-group">
			  <label class="control-label" for="pilihb">Pilihan B</label>
			  <div class="controls">
				<input autocomplete="off" id="pilihb" name="pilihb" placeholder="" class="input-xlarge" required="" type="text" value="">
				
			  </div>
			</div>

			<div class="control-group">
			  <label class="control-label" for="pilihc">Pilihan C</label>
			  <div class="controls">
				<input autocomplete="off" id="pilihc" name="pilihc" placeholder="" class="input-xlarge" required="" type="text" value="">
				
			  </div>
			</div>

			<div class="control-group">
			  <label class="control-label" for="pilihd">Pilihan D</label>
			  <div class="controls">
				<input autocomplete="off" id="pilihd" name="pilihd" placeholder="" class="input-xlarge" required="" type="text" value="">
				
			  </div>
			</div>

			<div class="control-group">
			  <label class="control-label" for="kunci">Kunci Jawaban</label>
			  <div class="controls">
				<input autocomplete="off" id="kunci" name="kunci" placeholder="" class="input-xlarge" required="" type="text" value="">
				<input type="hidden" name="idKuis" value="<?php echo $id_kuis; ?>" type="text">
			  </div>
			</div>

			<div class="control-group">
			  <label class="control-label" for="simpan"></label>
			  <div class="controls">
				<button type="submit" id="simpan" name="simpan" class="btn btn-success">Simpan</button>
				<button type="reset" id="batal" name="batal" class="btn btn-danger" onclick="javascript:window.location='view-soal-<?=$id_kuis ?>-<?=$r['jml_soal'] ?>?id=<?php echo $id_kuis ?>&j=<?=$r['jml_soal'] ?>';">Batal</button>
			  </div>
			</div>
		</fieldset>
		</form>
	</div>
	</div>
	<?php
	break;
	case "view-soal":
	$id_kuis=$_GET['id'];
	$jml=$_GET['j'];
	?>
	<div id="space" style="padding-top: 80px;"></div>
	<div class="tabelis" style="width:1160px; margin: 0 auto;">
	<?php
		$query=mysqli_query($con, "SELECT * FROM tbl_soal WHERE id_kuis=$id_kuis");
		$cek=mysqli_num_rows($query);

		if ($cek == $jml) {
			?>
			<a disabled class="btn">Tambah Soal Kuis</a>
			<?php
		} else {
			?>
			<a href="add-soal-<?=$id_kuis ?>?id=<?php echo $id_kuis ?>" class="btn">Tambah Soal Kuis</a>
			<?php
		}
	?>
		  <br />
		  <br />
		  <br />
		<table class="table table-striped table-condensed">
		<thead>
		<tr>
			<th>No.</th>
			<th>Pertanyaan</th>
			<th>Pilihan A</th>
			<th>Pilihan B</th>
			<th>Pilihan C</th>
			<th>Pilihan D</th>
			<th>Kunci Jawaban</th>
			<th>Aksi</th>
		</tr>
		</thead>
		<tbody>
		<?php 
			$tampil	=	mysqli_query($con, "SELECT tbl_soal.id_soal, tbl_soal.pertanyaan, tbl_soal.pilihan_a, tbl_soal.pilihan_b,
			tbl_soal.pilihan_c, tbl_soal.pilihan_d, tbl_soal.kunci FROM tbl_soal WHERE tbl_soal.id_kuis = $_GET[id]");
			$no=1;
		while ($r=mysqli_fetch_array($tampil)){
			?>
		<tr>
			<td><?= $no; ?></td>
			<td><?= $r['pertanyaan'] ?></td>
			<td><?= $r['pilihan_a'] ?></td>
			<td><?= $r['pilihan_b'] ?></td>
			<td><?= $r['pilihan_c'] ?></td>
			<td><?= $r['pilihan_d'] ?></td>
			<td><?= $r['kunci'] ?></td>
			<td><a class="button" href="homedos.php?detail=soaldos&act=edit-soal&id=<?=$r['id_soal'] ?>">Edit</a> <a class="redbutton" href="inc/aksisoaldos.php?detail=del-soal&act=hapus&id=<?=$r['id_soal'] ?>&idk=<?=$_GET['id'] ?>" onclick="return confirm('Are you sure want to delete this?')">Hapus</a></td>
		</tr>
			<?php
			$no++;
		}
		?>
		</tbody>
		</table>
		<button type="reset" id="batal" name="batal" class="btn btn-danger" onclick="javascript:window.location='soaldos';">Kembali</button>
	</div>
	<div id="space" style="padding-top: 80px;"></div>
	<?php
	break;
	case "edit-soal":
	?>
	<?php
	$edit	=	mysqli_query($con, "SELECT ts.*, tk.jml_soal FROM tbl_soal ts, tbl_kuis tk WHERE ts.id_kuis=tk.id_kuis and id_soal='$_GET[id]'");
		$r		=	mysqli_fetch_array($edit);
	?>
	<div id="space" style="padding-top: 80px;"></div>
	<div class="tabelis" style="width:860px; margin: 0 auto;">
		<form class="form-horizontal" action="inc/aksisoaldos.php?detail=edit-soal&act=edit" method="post">
			<fieldset>
			<legend>Tambah Soal Kuis</legend>
			<div class="control-group">
			  <label class="control-label" for="mk">Pilih Mata Kuliah</label>
			  <div class="controls">
			  <input type="hidden" name="id_soal" id="id_soal" value="<?=$r['id_soal'] ?>" />
				<input type="hidden" name="id_kuis" id="id_soal" value="<?=$r['id_kuis'] ?>" />
			  <?php
				$mk = mysqli_query($con, "SELECT tbl_mk.nama_mk FROM tbl_mk, 
				tbl_soal WHERE tbl_soal.id_mk = tbl_mk.id_mk AND tbl_soal.id_mk = '$r[id_mk]' GROUP BY tbl_mk.nama_mk");
				$t		=	mysqli_fetch_array($mk);
			  ?>
			  <input id="mk" name="mk" value="<?=$t['nama_mk'] ?>" disabled="disabled" class="input-xlarge" required="" type="text">
			  </div>
			</div>
			
			<div class="control-group">
			  <label class="control-label" for="tanya">Soal Pertanyaan</label>
			  <div class="controls">                     
				<textarea id="tanya" name="tanya"><?=$r['pertanyaan'] ?></textarea>
			  </div>
			</div>

			<div class="control-group">
			  <label class="control-label" for="piliha">Pilihan A</label>
			  <div class="controls">
				<input id="piliha" name="piliha" value="<?=$r['pilihan_a'] ?>" class="input-xlarge" required="" type="text">
			  </div>
			</div>

			<div class="control-group">
			  <label class="control-label" for="pilihb">Pilihan B</label>
			  <div class="controls">
				<input id="pilihb" name="pilihb" value="<?=$r['pilihan_b'] ?>" class="input-xlarge" required="" type="text">
				
			  </div>
			</div>

			<div class="control-group">
			  <label class="control-label" for="pilihc">Pilihan C</label>
			  <div class="controls">
				<input id="pilihc" name="pilihc" value="<?=$r['pilihan_c'] ?>" class="input-xlarge" required="" type="text">
				
			  </div>
			</div>

			<div class="control-group">
			  <label class="control-label" for="pilihd">Pilihan D</label>
			  <div class="controls">
				<input id="pilihd" name="pilihd" value="<?=$r['pilihan_d'] ?>" class="input-xlarge" required="" type="text">
				
			  </div>
			</div>

			<div class="control-group">
			  <label class="control-label" for="kunci">Kunci Jawaban</label>
			  <div class="controls">
				<input id="kunci" name="kunci" value="<?=$r['kunci'] ?>" class="input-xlarge" required="" type="text">
				
			  </div>
			</div>

			<div class="control-group">
			  <label class="control-label" for="simpan"></label>
			  <div class="controls">
				<button type="submit" id="simpan" name="simpan" class="btn btn-success">Simpan</button>
				<button type="reset" id="batal" name="batal" class="btn btn-danger" onclick="javascript:window.location='view-soal-<?=$r['id_kuis'] ?>-<?=$r['jml_soal'] ?>?id=<?php echo $r['id_kuis'] ?>&j=<?php echo $r['jml_soal'] ?>';">Batal</button>
			  </div>
			</div>
		</fieldset>
		</form>
	</div>
	</div>
	<?php
	break;
	case "buat-kuis":
	?>
	<div id="space" style="padding-top: 80px;"></div>
	<div class="tabelis" style="width:860px; margin: 0 auto;">
	<form autocomplete="off" class="form-horizontal" action="inc/aksisoaldos.php?detail=buat-kuis&act=buat" method="post">
	<fieldset>
	<legend>Buat Kuis</legend>
	<div class="control-group">
		<label class="control-label" for="mapel">Pilih Mata Kuliah</label>
		<div class="controls">
		<select id="idmk" name="idmk" class="input-xlarge">
		<?php
			$tampil	= mysqli_query($con, "SELECT * FROM tbl_mk WHERE id_user=$_SESSION[idUser]");
			while ($r = mysqli_fetch_array($tampil)){
				?>
				<option value="<?=$r['id_mk']?>"><?=$r['nama_mk'] ?></option>
				<?php
			}
		?>
		</select>
		</div>
	</div>
	
	<div class="control-group">
		<label class="control-label" for="nama_kuis">Nama Kuis</label>
		<div class="controls">                     
		<textarea autocomplete="off" id="nama_kuis" name="nama_kuis" style="resize: none" value=""></textarea>
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="nama_kuis">Jumlah Soal</label>
		<div class="controls">                     
		<input autocomplete="off" style="width: 55px" id="jml_soal" name="jml_soal" placeholder="" class="input-xlarge" required="" type="text" value="">
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="nama_kuis">Nilai Soal</label>
		<div class="controls">                     
		<input autocomplete="off" style="width: 55px" id="nilai_soal" name="nilai_soal" placeholder="" class="input-xlarge" required="" type="text" value=""> / soal
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="timer">Waktu Pengerjaan</label>
		<div class="controls">
			<input autocomplete="off" style="width: 55px" id="timer" name="timer" placeholder="" class="input-xlarge" required="" type="text" value=""> menit	
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="tanggal">Tanggal</label>
		<div class="controls">
		<input autocomplete="off" id="tanggal" name="tanggal" placeholder="" class="input-xlarge" required="" type="date" value="<?=date('Y-m-d')?>">
		</div>
	</div>

	<div class="control-group">
		<label class="control-label" for="simpan"></label>
		<div class="controls">
		<button type="submit" id="simpan" name="simpan" class="btn btn-success">Simpan</button>
		<button type="reset" id="batal" name="batal" class="btn btn-danger" onclick="javascript:window.location='soaldos';">Batal</button>
		</div>
	</div>
</fieldset>
</form>
	</div>
	</div>
	<?php
}
}
?>