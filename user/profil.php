<!DOCTYPE html>
<html>
	<head>
	<title>Update Profil - GO Practice!</title>

	<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="all">
	<div id="main">
	<div class="centerblock">
	<div class="register stepbystep container content-block">
	<div class="body">
		<form action="aksiprofil" method="post" name="reg_form" id="reg_form" class="forms">
		<?php
			include "../koneksi/konek.php";
			session_start();

			$query = mysqli_query($con, "SELECT * FROM tbl_profil WHERE id_user=$_SESSION[idUser]");
			$r = mysqli_fetch_array($query);

			if ($r['nama_profil'] == null) {
				?>
				<div class="section fullwidth">
				<div>
					<label for="nama">Nama Lengkap</label>
					<input name="nama_profil" type="text" value="" id="nama_profil" placeholder="Type your name" required>
				</div>
				</div>
				<div class="actions">
					<button name="submit" class="bbutton">UPDATE</button>
					<button name="batal" disabled class="bbutton">BATAL</button>
				</div>
				<?php
			} else {
				?>
				<div class="section fullwidth">
				<div>
					<label for="nama">Nama Lengkap</label>
					<input name="nama_profil" type="text" value="" id="nama_profil" placeholder="Type your name" required>
				</div>
				</div>
				<div class="actions">
					<button name="submit" class="bbutton">UPDATE</button>
					<a class="button" href="javascript:history.go(-1)">BATAL</a>
				</div>
				<?php
			}
		?>
		</form>
	</div>
	</div>
	</div>
	</div>
</div>
</body>
</html>