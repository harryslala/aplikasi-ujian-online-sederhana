<?php
session_start();
error_reporting(0);


if($_SESSION['login']==0){
  echo '
		<html>
		<head>
		<title>Login Berhasil</title>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<meta HTTP-EQUIV="REFRESH" content="3; url=../">
		<link href="style.css" rel="stylesheet" type="text/css" />
		</head>
		<body>
		<div id="all">
		<div id="main">
		<div class="centerblock">
		<div class="register stepbystep container content-block">
		<div class="body">
		<h2>Akses Di Tolak</h2>
		<p>Anda Berhasil Logout. Halaman Akan Redirect Dalam 3 Detik</p>
		</div>
		</div>
		</div>
		</div>
		</div>
		</body>
		</html>
  ';
}
else{
if (empty($_SESSION['username']) AND empty($_SESSION['password']) AND $_SESSION['login']==0){
   echo '
		<html>
		<head>
		<title>Login Berhasil</title>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<meta HTTP-EQUIV="REFRESH" content="3; url=../">
		<link href="style.css" rel="stylesheet" type="text/css" />
		</head>
		<body>
		<div id="all">
		<div id="main">
		<div class="centerblock">
		<div class="register stepbystep container content-block">
		<div class="body">
		<h2>Akses Di Tolak</h2>
		<p>Anda Berhasil Logout. Halaman Akan Redirect Dalam 3 Detik</p>
		</div>
		</div>
		</div>
		</div>
		</div>
		</body>
		</html>
  ';
}
else{
	include "../koneksi/konek.php";
?>
<!DOCTYPE html>
<head>
	<title>Download Materi - GO Practice!</title>
    
    <!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
    <!-- Stylesheets -->
    <link rel="stylesheet" href="css/basdat.css">
    <link href='http://fonts.googleapis.com/css?family=Ruda' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=PT+Sans' rel='stylesheet' type='text/css'>
    
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
    
    <noscript><link rel="stylesheet" href="css/no-js.css"></noscript>
    
    <!-- Favicons -->
	<link rel="shortcut icon" href="images/favicon2.png">
    
    <!-- JavaScript -->
    <script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>
    <script type='text/javascript' src='js/bootstrap.min.js'></script>
    <script type="text/javascript" src="js/jquery-easing.js"></script>
    <script type='text/javascript' src='js/jquery.placeholder.min.js'></script>
	<script type='text/javascript' src='js/jquery.flexslider-min.js'></script>
    <script type="text/javascript" src="js/main.js"></script>
    
</head>

<body>

 <div id="header">
 <?php
    $query = mysqli_query($con, "SELECT * FROM tbl_profil WHERE id_user=$_SESSION[idUser]");
    $r = mysqli_fetch_array($query);
 ?>
  <div class="container">
   <div class="row-fluid">
    
    <div class="span1"></div>
    <nav class="nav-menu span10">
     <ul>
      <li style="float: left;"><a href="mhshome">Home</a></li>
	  <div class="dropdown">
        <button class="dropbtn"><?php echo $r['nama_profil'] ?></button>
            <div class="dropdown-content">
                <ul>
                <a href="profil">Profil</a>
                <a href="logout">Logout</a>
                </ul>
            </div>
     </div>
	  <li><a href="materi">Download Materi</a></li>
	  <li><a href="nilai">Rekap Nilai</a></li>
     </ul>
    </nav>
    <div class="span1"></div>
    
   </div> <!-- End Row Fluid -->
  </div> <!-- End Container -->
 </div> <!-- End Header -->
 <br />
 <br />
 <br />
 <div id="main">
  <div class="tabelis" style="width:860px; margin: 0 auto;margin-top: 40px;">
  <legend>Download Materi Belajar</legend>
  <table class="table table-striped table-condensed">
	<thead>
	<tr>
		<th>No.</th>
		<th>Nama Materi</th>
		<th>Dosen</th>
		<th>Keterangan</th>
		<th>Aksi</th>
	</tr>
	</thead>
	<tbody>
	<?php 
		$tampil	=	mysqli_query($con, "SELECT tbl_materi.id_materi,tbl_materi.id_user,tbl_materi.nama_materi,tbl_materi.file,tbl_materi.keterangan,tbl_profil.nama_profil,tbl_profil.jurusan FROM tbl_materi, tbl_profil WHERE tbl_materi.id_user=tbl_profil.id_user AND tbl_profil.jurusan='$_SESSION[jur]' ORDER BY id_materi DESC");
		$no=1;
    while ($r=mysqli_fetch_array($tampil)){
		?>
	<tr>
		<td><?= $no; ?></td>
		<td><?= $r['nama_materi'] ?></td>
		<td><?= $r['nama_profil'] ?></a></td>
		<td><?= $r['keterangan'] ?></td>
		<td><a class="button" href="../file/<?= $r['file'] ?>">Download</a></td>
	</tr>
		<?php
		$no++;
	}
	?>
	</tbody>
	</table>
 <br />
 </div>
 </div> <!-- End Main -->
 <br />
 <br />
 <br />
 <div id="footer">
  <div class="container">
   
   <div class="copyright-text pull-left">Copyright &copy; 2013 by bye. All right!</div> <!-- Copyright Text -->
   
   <ul class="social pull-right">
    <li><a href="#" title="Twitter"><img src="images/social/twitter.png" alt="Twitter" /></a></li>
    <li><a href="#" title="Facebook"><img src="images/social/facebook500.png" alt="Facebook" /></a></li>
    <li><a href="#" title="Google Plus"><img src="images/social/googleplus.png" alt="Google Plus" /></a></li>
   </ul> <!-- End Social Media -->
   
  </div> <!-- End Container -->
 </div> <!-- End Footer -->

</body>
</html>
<?php
}
}
?>