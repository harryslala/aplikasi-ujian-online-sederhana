<?php
include "../koneksi/konek.php";

$username = $_POST['username'];
$username = mysqli_real_escape_string($con,$username);
$pass = md5($_POST['password']);
$pass     = mysqli_real_escape_string($con,$pass);

if (!ctype_alnum($username) OR !ctype_alnum($pass)){
  echo "Injection not allowed!";
}
else{

	$masuk = mysqli_query($con, "SELECT tu.id_user,tu.username,tu.password,tu.status,tp.nama_profil,tp.jurusan FROM tbl_user tu JOIN tbl_profil tp ON tp.id_user=tu.id_user WHERE tu.username='$username' AND tu.password='$pass'");
	$find		=	mysqli_num_rows($masuk);
	$r=mysqli_fetch_array($masuk);

	if ($find > 0){
		if($r['status'] == "mahasiswa") {
			session_start();
			$_SESSION['idUser'] = $r['id_user'];
			$_SESSION['username'] = $r['username'];
			$_SESSION['password'] = $r['password'];
			$_SESSION['jur'] = $r['jurusan'];
			$_SESSION['login'] = 1;

			if ($r['nama_profil'] === null) {
				echo '
					<html>
					<head>
					<title>Login Berhasil</title>
					<meta http-equiv="content-type" content="text/html;charset=utf-8" />
					<meta HTTP-EQUIV="REFRESH" content="2; url=profil">
					<link href="style.css" rel="stylesheet" type="text/css" />
					</head>
					<body>
					<div id="all">
					<div id="main">
					<div class="centerblock">
					<div class="register stepbystep container content-block">
					<div class="body" style="text-align:center;">
					<h2>Login Berhasil</h2>
					<p>Selamat Datang <strong>'.$username.'</strong>!</p>
					</div>
					</div>
					</div>
					</div>
					</div>
					</body>
					</html>
			';
			} else {
				echo '
					<html>
					<head>
					<title>Login Berhasil</title>
					<meta http-equiv="content-type" content="text/html;charset=utf-8" />
					<meta HTTP-EQUIV="REFRESH" content="2; url=mhshome">
					<link href="style.css" rel="stylesheet" type="text/css" />
					</head>
					<body>
					<div id="all">
					<div id="main">
					<div class="centerblock">
					<div class="register stepbystep container content-block">
					<div class="body" style="text-align:center;">
					<h2>Login Berhasil</h2>
					<p>Selamat Datang <strong>'.$r['nama_profil'].'</strong>!</p>
					</div>
					</div>
					</div>
					</div>
					</div>
					</body>
					</html>
			';
			}
		}
		if($r['status'] == "dosen") {
			session_start();
			$_SESSION['idUser'] = $r['id_user'];
			$_SESSION['username']     = $r['username'];
			$_SESSION['password']     = $r['password'];
			$_SESSION['jur'] = $r['jurusan'];
			$_SESSION['login'] 			= 1;
			
			if ($r['nama_profil'] === null) {
				echo '
					<html>
					<head>
					<title>Login Berhasil</title>
					<meta http-equiv="content-type" content="text/html;charset=utf-8" />
					<meta HTTP-EQUIV="REFRESH" content="3; url=profil">
					<link href="style.css" rel="stylesheet" type="text/css" />
					</head>
					<body>
					<div id="all">
					<div id="main">
					<div class="centerblock">
					<div class="register stepbystep container content-block">
					<div class="body" style="text-align:center;">
					<h2>Login Berhasil</h2>
					<p>Selamat Datang <strong>'.$username.'</strong>...</p>
					</div>
					</div>
					</div>
					</div>
					</div>
					</body>
					</html>
			';
			} else {
				echo '
					<html>
					<head>
					<title>Login Berhasil</title>
					<meta http-equiv="content-type" content="text/html;charset=utf-8" />
					<meta HTTP-EQUIV="REFRESH" content="3; url=doshome">
					<link href="style.css" rel="stylesheet" type="text/css" />
					</head>
					<body>
					<div id="all">
					<div id="main">
					<div class="centerblock">
					<div class="register stepbystep container content-block">
					<div class="body" style="text-align:center;">
					<h2>Login Berhasil</h2>
					<p>Selamat Datang <strong>'.$r['nama_profil'].'</strong>!</p>
					</div>
					</div>
					</div>
					</div>
					</div>
					</body>
					</html>
			';
			}
		}
	
	}
else{
 echo '
		<html>
		<head>
		<title>Login Gagal</title>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<meta HTTP-EQUIV="REFRESH" content="2; url=../login">
		<link href="style.css" rel="stylesheet" type="text/css" />
		</head>
		<body>
		<div id="all">
		<div id="main">
		<div class="centerblock">
		<div class="register stepbystep container content-block">
		<div class="body" style="text-align:center;">
		<h2>Login Gagal</h2>
		<p>Login Gagal! Silahkan Cek Username / Password Anda.</p>
		</div>
		</div>
		</div>
		</div>
		</div>
		</body>
		</html>
  ';
}
}
?>
