<!DOCTYPE html>
<html>
	<head>
	<title>Pendaftaran - GO Practice!</title>

	<link href="style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="all">
	<div id="main">
	<div class="centerblock">
	<div class="register stepbystep container content-block">
	<div class="body">
		<form action="auth" method="post" name="reg_form" id="reg_form" class="forms">
			<div class="section fullwidth">
				<div>
					<label for="username">USERNAME*</label>
					<input name="username" type="text" value="" id="username" placeholder="Type Username" required>
				</div>
				<div>
					<label for="password">PASSWORD*</label>
					<input name="password" type="password" value="" id="password" placeholder="Type Password" required>				
				</div>
				<div>
					<label for="status">STATUS*</label>
					<select name="status" id="status" required>
						<option value="" selected disabled hidden>Pilih Status :</option>
  						<option value="mahasiswa">Mahasiswa</option>
  						<option value="dosen">Dosen</option>
 						<!-- other options from your database query results displayed here -->
					</select>
				</div>
				<div>
					<label for="fakultas" id='fak_field'>Fakultas</label>
					<select name="fak" id="fakultas" required>
						<option value="" selected disabled hidden>Pilih Fakultas:</option>
  						<option value="fte">Teknik Elektro</option>
 						<!-- other options from your database query results displayed here -->
					</select>
				</div>
				<div>
					<label for="jurusan" id='jur_field'>Jurusan</label>
					<select name="jur" id="jurusan" required>
						<option value="" selected disabled hidden>Pilih Jurusan:</option>
						<option value="sk">S1 Sistem Komputer</option>
  						<option value="te">S1 Teknik Elektro</option>
  						<option value="tt">S1 Teknik Telekomunikasi</option>
						<option value="tf">S1 Teknik Fisika</option>
 						<!-- other options from your database query results displayed here -->
					</select>
				</div>
			</div>
			<div class="actions">
				<button name="submit" class="bbutton">DAFTAR</button>
			</div>
			<div class="actions">
				<center><a href="../basdat/">Home</a></center>
			</div>
		</form>
	</div>
	</div>
	</div>
	</div>
</div>
</body>
</html>